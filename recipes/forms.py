from . import models
from django.forms import ModelForm, TextInput
from django.utils.translation import ugettext_lazy as _


class RecipeForm(ModelForm):
    class Meta:
        model = models.Recipe
        fields = ['name', 'servings', 'preparationTime', 'cookingTime', 'difficulty', 'notes']
        labels = {
            'name': _('Name'),
            'servings': _('Servings'),
            'preparationTime': _('Preparation Time'),
            'cookingTime': _('Cooking Time'),
            'difficulty': _('Difficulty'),
            'notes': _('Notes')
        }


class IngredientForm(ModelForm):
    class Meta:
        model = models.Ingredient
        fields = ['product', 'amount', 'unit']
        labels = {
            'product': _('Product'),
            'amount': _('Amount'),
            'unit': _('Unit'),
            }
        widgets = {
            'product': TextInput(),
            'unit': TextInput()
        }
