import json

CONFIG_FILE = 'recipes.config'


def load(key):
    return json.loads(open(CONFIG_FILE).read()).get(key)
