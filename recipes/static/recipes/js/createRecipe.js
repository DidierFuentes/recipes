function attachRemoveIngredient(){
    $('[data-id="removeIngredient"]').click(function(){
        $(this).parent().parent().remove();
    });
}

function attachRemoveStep(){
    $('[data-id="removeStep"]').click(function(){
        $(this).parent().parent().remove();
    });
}

$(document).ready(function(){
    ingredientForm = $(".added-ingredient").clone();
    stepForm = $(".added-step").clone();
    attachRemoveIngredient();
    attachRemoveStep();
    productsAutocomplete();
    unitsAutocomplete();
});

$('[data-id="addIngredient"]').click(function(e){
    e.preventDefault();
    var newIng = ingredientForm.clone();
    var form = $('.ingredientRow');

    var formName = form.find('[name="product"]');
    var formAmount = form.find('[name="amount"]');
    var formUnit = form.find('[name="unit"]');

    if (ingredientIsInvalid(form)) {
        return false;
    }

    var json_data = JSON.stringify({
        'product':  formName.attr('data-id'),
        'amount': formAmount.val(),
        'unit': formUnit.attr('data-id')
    })
    console.log(json_data)
    $.ajax({
        url: '/rest/v1/validateIngredient',
        method: 'post',
        data: {
            'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val(),
            'ingredient': json_data
        }
    })
    .done(function(data, textStatus, jqXHR){

        console.log(data);
        console.log(textStatus);
        console.log(jqXHR);

        newIng.find('.ingredient-name').text(formName.val());
        newIng.find('.ingredient-name').attr('data-id', formName.attr('data-id'));
        newIng.find('.ingredient-amount').text(formAmount.val());
        newIng.find('.ingredient-unit').text(formUnit.val());
        newIng.find('.ingredient-unit').attr('data-id', formUnit.attr('data-id'));

        formName.val('');
        formAmount.val('');
        formUnit.val('');

        form.before(newIng);
        newIng.show();

        attachRemoveIngredient();
        productsAutocomplete();
        unitsAutocomplete();
    })
    .fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.responseJSON);
    });
    return false;
});

$('[data-id="addStep"]').click(function(){
    var newStep = stepForm.clone();
    var form = $('.stepRow');

    var stepContent = form.find('#id_step');

    newStep.find('.step-label').text(stepContent.val());

    stepContent.val('');

    form.before(newStep);
    newStep.show();

    attachRemoveStep();
    return false;
});

$('#saveBtn').click(function(){
    var data = {
        'name': $('#id_name').val(),
        'servings': $('#id_servings').val(),
        'preparationTime': $('#id_preparationTime').val(),
        'cookingTime': $('#id_cookingTime').val(),
        'notes': $('#id_notes').val(),
        'difficulty': $('#id_difficulty').val(),
        'ingredients': [],
        'steps': []
    }
    $('.added-ingredient:visible').each(function(){
        data['ingredients'].push({
            'product': $(this).find('.ingredient-name').attr('data-id'),
            'amount': $(this).find('.ingredient-amount').text(),
            'unit': $(this).find('.ingredient-unit').attr('data-id'),
        });
    });
    var stepNo = 1;
    $('.added-step:visible').each(function(){
        data['steps'].push({
            'number': stepNo,
            'content': $(this).find('.step-label').text()
        });
        stepNo += 1;
    });
    $.ajax({
        url: '/rest/v1/saveRecipe/',
        method: 'post',
        data: {
            'data': JSON.stringify(data),
            'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val()
            }
    })
    .done(function(data, textStatus, jqXHR){
        alert('Recipe saved successfully!');
    });

});

function productsAutocomplete(){
    var query = '';
    $('[name="product"]').autocomplete({
        source:function(request, response){
            query = request.term;
            $.ajax({
                url: '/products/autocomplete?q=' + request.term,
                method: 'get'
            }).done(function(data, textStatus, jqXHR){
                console.log(data.products);
                response(data.products);
            });
        },
        select: function(event, ui){
            $(this).val(ui.item.value);
            $(this).attr('data-id', ui.item.id);
            return false;
        }
    })
    .autocomplete("instance")._renderItem = function(ul, item){
        var regex = new RegExp(query, "gi");
        return $("<li>")
        .append( "<div>" + item.value.replace(regex, "<b>" + query + "</b>") + "</div>" )
        .appendTo( ul );
    };
}

function unitsAutocomplete(){
    var query = '';
    $('[name="unit"]').autocomplete({
        source:function(request, response){
            query = request.term;
            $.ajax({
                url: '/rest/v1/unitsAutocomplete?q=' + request.term,
                method: 'get'
            }).done(function(data, textStatus, jqXHR){
                response(data.units);
            });
        },
        select: function(event, ui){
            $(this).val(ui.item.value);
            $(this).attr('data-id', ui.item.id);
            return false;
        }
    })
    .autocomplete("instance")._renderItem = function(ul, item){
        var regex = new RegExp(query, "gi");
        return $("<li>")
        .append( "<div>" + item.value.replace(regex, "<b>" + query + "</b>") + "</div>" )
        .appendTo( ul );
    };
}

function ingredientIsInvalid(ingredient) {

    var name = ingredient.find('[name="product"]');
    var amount = ingredient.find('[name="amount"]');
    var unit = ingredient.find('[name="unit"]');

    var invalid = false;

    unit.parent().removeClass('has-error');
    amount.parent().removeClass('has-error');
    name.parent().removeClass('has-error');

    // Validate the ingredient's unit
    if (unit.val() == '' || typeof unit.attr('data-id') === "undefined") {
        console.log('invalid unit');
        unit.parent().addClass('has-error');
        invalid = true;
        unit.focus();
    }

    // Validate the ingredient's amount
    if (amount.val() == '' || amount.val() <= 0) {
        console.log('invalid amount');
        amount.parent().addClass('has-error');
        invalid = true;
        amount.focus();
    }

    // Validate the ingredient's name
    if (name.val() == '' || typeof name.attr('data-id') === "undefined") {
        console.log('invalid name');
        name.parent().addClass('has-error');
        invalid = true;
        name.focus();
    }

    return invalid;

}