$('#password').keyup(function(event){
    if(event.keyCode == 13){
        $("#loginBtn").click();
    }
});

$('#loginBtn').click(function(){
    var data = {
                'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val(),
                'username': $('#username').val(),
                'password': $('#password').val()
               }
    console.log(data);
    $.ajax({
        url: '/rest/v1/login',
        method: 'post',
        data: data
    })
    .done(function(data, textStatus, jqXHR) {
        if(jqXHR.status == 200){
            afterLogin();
        }
    }).fail(function(data, textStatus, jqXHR){
        if(data.status == 401){
            alert('Incorrect user and/or password.');
        }
    });
});

function onGoogleSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    data = {
        'Id_token': googleUser.getAuthResponse().id_token
    }
    $.ajax({
        url: '/users/login/google/',
        method: 'post',
        data: {'token': googleUser.getAuthResponse().id_token}
    }).done(function(data, textStatus, jqXHR){
        if(jqXHR.status == 200){
            afterLogin();
        }
    }).fail(function(data, textStatus, jqXHR){
        if(data.status == 401){
            alert('This google account is invalid.');
        }
    });
}

function afterLogin() {
    var next = getUrlParameter('next');
    if (next) {
        window.location.href = next;
    } else {
        window.location.href = '/recipes'
    }
}