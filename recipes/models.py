from django.db import models
from django.db.models import Model
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

import recipes.validators as v
from products.models import Product


class Recipe(Model):
    DIFFICULTIES = (
        ('easy', _('Easy')),
        ('med', _('Medium')),
        ('hard', _('Hard'))
    )

    dateRegistered = models.DateField()
    name = models.CharField(max_length=30, validators=[v.validate_recipe_name])
    servings = models.IntegerField(default=1, validators=[v.validate_recipe_servings])
    difficulty = models.CharField(max_length=4, choices=DIFFICULTIES)
    notes = models.CharField(max_length=500, blank=True)
    author = models.ForeignKey(User)
    preparationTime = models.IntegerField(default=0)
    cookingTime = models.IntegerField(default=0)
    products = models.ManyToManyField(Product, through='Ingredient')

    def get_difficulty(self):
        return [dif[1] for dif in self.DIFFICULTIES if dif[0] == self.difficulty][0]

    def __str__(self):
        return '<Recipe(name={})>'.format(self.name)


class MeasurementUnit(Model):
    name = models.CharField(max_length=25)
    short_name = models.CharField(max_length=8)

    def __str__(self):
        return '<MeasurementUnit(name={})>'.format(self.name)


class Ingredient(Model):
    recipe = models.ForeignKey(Recipe)
    product = models.ForeignKey(Product)
    amount = models.DecimalField(decimal_places=2, max_digits=4, validators=[v.validate_ingredient_amount])
    unit = models.ForeignKey(MeasurementUnit)

    def __str__(self):
        return '<Ingredient(recipe={}, product={})>'.format(self.recipe.name, self.product.name)

    def to_dict(self):
        """
        Extends __dict__ method by adding id value of foreign key fields
        :return:
        """
        result = super(Ingredient, self).__dict__
        result['product'] = self.product.id
        result['unit'] = self.unit.id

        return result


class Step(Model):
    recipe = models.ForeignKey(Recipe)
    number = models.IntegerField(default=1)
    content = models.CharField(max_length=500, validators=[v.validate_step_content])

    def __str__(self):
        return '<Step(recipe={}, number={})>'.format(self.recipe.name, self.number)

    def save(self, **kwargs):
        self.full_clean()
        super(Step, self).save(kwargs)


class Image(Model):
    recipe = models.ForeignKey(Recipe)
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=50)
