from django.contrib.auth import logout as django_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.generic import TemplateView, ListView, RedirectView
from rest_framework import permissions

from recipes import models
from . import forms


class LoginView(TemplateView):
    template_name = 'login.html'


class LogoutView(RedirectView):
    def get(self, request, *args, **kwargs):
        django_logout(request)
        self.url = '/login'
        return super(LogoutView, self).get(request, args, **kwargs)


class RecipesView(ListView):
    model = models.Recipe
    template_name = 'viewRecipes.html'

    def get_context_data(self, **kwargs):
        context = super(RecipesView, self).get_context_data(**kwargs)
        context['title'] = _('Recipes')
        return context


class RecipeDetailView(TemplateView):
    permission_classes = (permissions.IsAuthenticated, )
    template_name = 'recipeDetail.html'

    def get_context_data(self, **kwargs):
        context = super(RecipeDetailView, self).get_context_data(**kwargs)
        recipe = models.Recipe.objects.get(id=kwargs.get('pk'))
        context['recipe'] = recipe
        context['ingredients'] = models.Ingredient.objects.filter(recipe=recipe)
        context['steps'] = models.Step.objects.filter(recipe=recipe).order_by('number')
        return context


# @login_required
# def create_recipe_view(request):
#
#     return render(request, 'createRecipe.html', {'form': forms.RecipeForm,
#                                                  'ingForm': forms.IngredientForm})

class CreateRecipeView(TemplateView):
    permission_classes = (permissions.IsAuthenticated, )
    template_name = 'createRecipe.html'

    @method_decorator(login_required)
    def get(self, *args, **kwargs):
        if not self.request.user.has_perm('recipes.add_recipe'):
            raise PermissionDenied
        return super(CreateRecipeView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CreateRecipeView, self).get_context_data(**kwargs)
        context['form'] = forms.RecipeForm
        context['ingForm'] = forms.IngredientForm
        return context


class UserRecipeView(ListView):
    model = models.Recipe
    template_name = 'viewRecipes.html'

    def get_context_data(self, **kwargs):
        context = super(UserRecipeView, self).get_context_data(**kwargs)
        context['title'] = (_('%s\'s recipes') % self.request.GET.get('username'))
        return context

    def get_queryset(self):
        author = User.objects.get(username=self.request.GET.get('username'))
        return models.Recipe.objects.filter(author=author).order_by('dateRegistered')


# Errors

class NotFound(TemplateView):
    template_name = 'errors/notFound404.html'


class Forbidden(TemplateView):
    template_name = 'errors/forbidden403.html'
