from recipes.configurations_loader import load
from django.apps import AppConfig
from recipes import settings


class RecipesConfig(AppConfig):
    name = 'recipes'
    path_config = 'win_path' if settings.DEBUG else 'recipes_path'
    path = load(path_config)
