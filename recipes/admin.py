from django.contrib import admin

# Register your models here.
from recipes.models import MeasurementUnit

admin.site.register(MeasurementUnit)
