"""recipes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import RedirectView

import recipes.views as views
from recipesWS.urls import urlpatterns as api_urls
from recipesWS.apps import RecipeswsConfig

from user_management.urls import urlpatterns as users_urls
from user_management.apps import UserManagementConfig

from products.urls import urlpatterns as products_urls
from products.apps import ProductsConfig

from recipes_help.urls import urlpatterns as help_urls
from recipes_help.apps import HelpConfig

handler404 = views.NotFound.as_view()
handler403 = views.Forbidden.as_view()

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', RedirectView.as_view(url='/recipes')),

    # Login
    url(r'^login/?$', views.LoginView.as_view()),
    url(r'^logout/?$', views.LogoutView.as_view()),

    # Recipes
    url(r'^recipes/?$', views.RecipesView.as_view()),
    url(r'^recipes/new/?$', views.CreateRecipeView.as_view()),
    url(r'^recipes/(?P<pk>[0-9]*)/?$', views.RecipeDetailView.as_view()),
    url(r'^recipes/user(?P<id>[0-9]*)/?$', views.UserRecipeView.as_view()),

    # Products
    url(r'^products/', (products_urls, 'products', ProductsConfig.name)),

    # Help
    url(r'^help/', (help_urls, 'help', HelpConfig.name)),

    url(r'^rest/v1/', (api_urls, 'recipesWS', RecipeswsConfig.name)),

    url(r'^users/', (users_urls, 'users', UserManagementConfig.name))
]
