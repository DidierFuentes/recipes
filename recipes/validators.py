from django.core.exceptions import ValidationError

from django.utils.translation import ugettext_lazy as _


def validate_recipe_name(name):
    if len(name) < 3:
        raise ValidationError(_('Recipe name must have 3 or more characters.'))

    return name


def validate_recipe_servings(servings):
    if servings <= 0:
        raise ValidationError(_('Recipe must serve at least 1.'))

    return servings


def validate_step_content(content):
    if len(content) < 10:
        raise ValidationError(_('Step content must have at least 10 characters.'))

    return content


def validate_step_number(number):
    if number <= 0:
        raise ValidationError(_('Step number must be greater than 1.'))


def validate_ingredient_amount(amount):
    if amount < 0:
        raise ValidationError(_('Amount must be greater than 0.'))
