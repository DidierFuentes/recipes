$('#saveBtn').click(function(){
    console.log('saving');
    var data = {
        'productName': $('#id_name').val()
    }
    $.ajax({
        url: '/products/save',
        method: 'post',
        data: {
            'csrfmiddlewaretoken': $('[name=csrfmiddlewaretoken]').val(),
            'product': JSON.stringify(data)
        }
    }).done(function(data, textStatus, jqXHR){
        alert('Product saved!');
    });
});