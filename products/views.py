import json

from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.views import APIView

from logs import ProductLog
from products import forms, models


class ProductsView(TemplateView):
    template_name = 'viewProducts.html'
    model = models.Product

    def get_context_data(self, **kwargs):
        context = super(ProductsView, self).get_context_data(**kwargs)
        context['products'] = models.Product.objects.all().order_by('name')
        return context


class CreateProductView(TemplateView):
    permission_classes = (permissions.IsAuthenticated,)
    template_name = 'createProduct.html'

    @method_decorator(login_required)
    def get(self, *args, **kwargs):
        if not self.request.user.has_perm('recipes.add_product'):
            raise PermissionDenied

        return super(CreateProductView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CreateProductView, self).get_context_data(**kwargs)
        context['form'] = forms.ProductForm
        return context


class ProductDetailView(TemplateView):
    template_name = 'productDetail.html'

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        context['product'] = models.Product.objects.get(id=kwargs.get('pk'))
        return context


class SaveProduct(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @staticmethod
    def post(request):
        if not request.user.has_perm('recipes.can_add_products'):
            return PermissionDenied

        data = json.loads(request.data.get('product'))

        product = models.Product(name=data.get('productName'))
        product.save()

        ProductLog.create(product, request.user)
        return Response(status=status.HTTP_200_OK)


class ProductsAutocomplete(APIView):
    @staticmethod
    def get(request):

        result = models.Product.objects.all()

        if request.GET.get('q'):
            result = result.filter(name__icontains=request.GET.get('q'))

        formatted_data = []

        for prod in result:
            formatted_data.append({
                'id': prod.id,
                'value': prod.name
            })

        return Response({'products': formatted_data}, status=status.HTTP_200_OK)

    def get_queryset(self):

        result = models.Product.objects.all()

        if self.request.GET.get('q'):
            result = result.filter(name__icontains=self.request.GET.get('q'))

        return result
