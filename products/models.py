from django.db import models
from django.db.models import Model


class Product(Model):
    name_length = 50
    name = models.CharField(verbose_name='English Name', max_length=name_length)
    es_name = models.CharField(verbose_name='Spanish Name', max_length=name_length)
    default_measurement = models.ForeignKey('recipes.MeasurementUnit')

    def __str__(self):
        return '<Product(name={})>'.format(self.name)
