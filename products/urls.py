"""Products URL Configuration
"""
from django.conf.urls import url

import products.views as views

urlpatterns = [
    url(r'^$', views.ProductsView.as_view()),

    url(r'^new/?$', views.CreateProductView.as_view()),
    url(r'^(?P<pk>[0-9]*)/?$', views.ProductDetailView.as_view()),

    # API
    url(r'save', views.SaveProduct.as_view()),
    url(r'autocomplete', views.ProductsAutocomplete.as_view()),
]
