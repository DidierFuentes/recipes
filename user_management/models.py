from django.db import models
from django.contrib.auth.models import User

from recipes import settings


class UserProfile(models.Model):
    # User
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    email_confirmed = models.BooleanField(default=False)

    # Social Networks
    google_id = models.CharField(max_length=50, blank=True, null=True)


class UserSettings(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='settings')
    language = models.CharField(max_length=4, choices=settings.LANGUAGES, default='en')
