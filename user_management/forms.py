from django.forms import ModelForm
from .models import UserSettings


class SettingsForm(ModelForm):
    class Meta:
        model = UserSettings
        fields = ['language']
