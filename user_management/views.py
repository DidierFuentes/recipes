import json

from django.contrib.auth import login as django_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.utils import translation
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView
from oauth2client import client, crypt
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from logs import GoogleLog
from recipes import settings
from user_management.models import UserProfile, UserSettings
from .forms import SettingsForm

# Create your views here.


class LoginWithGoogle(APIView):
    permission_classes = (permissions.AllowAny,)
    authentication_classes = ()

    @staticmethod
    def post(request):
        try:
            id_info = client.verify_id_token(request.data.get('token'), settings.GOOGLE_CLIENT_ID)

            if id_info.get('iss') not in ['accounts.google.com', 'https://accounts.google.com']:
                raise crypt.AppIdentityError("Wrong issuer.")

        except crypt.AppIdentityError:
            # Invalid token
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        # print(id_info)

        # Check if google id is associated to a registered user.
        try:
            user_profile = UserProfile.objects.get(google_id=id_info.get('sub'))

            # If google id is already registered
            if user_profile is not None:
                django_login(request, user_profile.user)
                GoogleLog.login(user_profile.user)
                return Response(status=status.HTTP_200_OK)

        except ObjectDoesNotExist:
            pass

        # If an user is registered with this email and it has been confirmed
        try:
            user = User.objects.get(email=id_info.get('email'))

            if user is not None:
                if user.profile.email_confirmed:
                    user.profile.google_id = id_info.get('sub')
                    user.profile.save()

                    django_login(request, user)
                    GoogleLog.email_link(user)
                    return Response(status=status.HTTP_200_OK)

                else:
                    # Email exists but hasn't been confirmed
                    return Response(status=status.HTTP_401_UNAUTHORIZED)

        except ObjectDoesNotExist:
            pass

        # Try to create a new user

        user = User.objects.create_user(
            username=id_info.get('email'),
            email=id_info.get('email'),
            first_name=id_info.get('given_name'),
            last_name=id_info.get('family_name')
        )
        user.save()
        profile = UserProfile(
            user=user,
            email_confirmed=True,
            google_id=id_info.get('sub')
        )
        profile.save()
        user_settings = UserSettings(
            user=user,
            language='en'
        )
        user_settings.save()
        django_login(request, user)
        GoogleLog.user_signup(user)
        return Response(status=status.HTTP_200_OK)


@method_decorator(login_required, "get")
class UserProfileView(TemplateView):
    template_name = 'profile.html'

    def get_context_data(self, user="", **kwargs):
        context = super(UserProfileView, self).get_context_data(**kwargs)

        if user == "":
            context['user'] = self.request.user
        else:
            try:
                context['user'] = User.objects.get(username=user)
            except ObjectDoesNotExist:
                raise Http404

        return context


@method_decorator(login_required, "get")
class UserSettingsView(TemplateView):
    template_name = 'settings.html'

    def get_context_data(self, **kwargs):
        context = super(UserSettingsView, self).get_context_data(**kwargs)
        user_settings = UserSettings.objects.get(user=self.request.user)
        context['form'] = SettingsForm(instance=user_settings)
        return context


class SaveUserSettings(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @staticmethod
    def post(request):
        data = json.loads(request.data.get('settings'))

        new_settings = UserSettings.objects.get(user=request.user)

        new_settings.language = data.get('lang')
        new_settings.save()

        # Set language setting
        translation.activate(new_settings.language)
        request.session[translation.LANGUAGE_SESSION_KEY] = new_settings.language

        return Response(status=status.HTTP_200_OK)
