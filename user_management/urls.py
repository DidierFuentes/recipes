"""user management URL Configuration
"""
from django.conf.urls import url

import user_management.views as views

urlpatterns = [
    url(r'profile/(?P<user>[a-zA-Z0-9.@]*)/?$', views.UserProfileView.as_view()),
    url(r'settings/?$', views.UserSettingsView.as_view()),

    # API
    url(r'login/google/?$', views.LoginWithGoogle.as_view()),
    url(r'settings/save/?$', views.SaveUserSettings.as_view()),
]
