$('[data-id="saveBtn"]').click(function(){
    lang = $('#id_language').val();
    $.ajax({
        url: '/users/settings/save',
        method: 'post',
        data: {
            'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val(),
            'settings': JSON.stringify({
                'lang': lang
                })
            }
    })
    .done(function(data, textStatus, jqXHR){
        location.reload(true);
    });
});