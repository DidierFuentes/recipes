from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'login', views.Login.as_view()),
    url(r'saveRecipe', views.SaveRecipe.as_view()),

    url(r'validateIngredient', views.ValidateIngredient.as_view()),

    # Autocomplete
    url(r'unitsAutocomplete', views.UnitsAutocomplete.as_view()),
]
