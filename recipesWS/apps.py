from django.apps import AppConfig


class RecipeswsConfig(AppConfig):
    name = 'recipesWS'
    verbose_name = 'Recipes Web Service'
