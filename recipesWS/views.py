import datetime
import json

from django.contrib.auth import authenticate, login as django_login
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from rest_framework import status, permissions, authentication
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from rest_framework.views import APIView

from logs import UserLog, RecipeLog, ProductLog
from recipes.forms import RecipeForm, IngredientForm
from recipes.models import Recipe, Step, Ingredient, Product, MeasurementUnit
from user_management.models import UserSettings, UserProfile


class Login(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.AllowAny,)

    @staticmethod
    def post(request):
        """

        :return:
        """
        username = request.data.get('username')
        password = request.data.get('password')

        user = authenticate(username=username, password=password)
        if user is None:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            django_login(request, user)
            try:
                UserSettings.objects.get(user=user)
            except ObjectDoesNotExist:
                user.settings = UserSettings(user=user)
                user.settings.save()

            try:
                UserSettings.objects.get(user=user)
            except ObjectDoesNotExist:
                user.profile = UserProfile(user=user)
                user.profile.save()

            translation.activate(user.settings.language)
            request.session[translation.LANGUAGE_SESSION_KEY] = user.settings.language

            UserLog.login(user)
            return Response(status=status.HTTP_200_OK)


class SaveRecipe(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @staticmethod
    def post(request):
        """
        Validates and saves the recipe sent in the request.
        
        :param request: contains the form with the recipes' data.
        :return: 200: if the recipe was saved successfully.
        :return: 400: if any of the validations failed.
        """
        form = request.data.get('data')

        if isinstance(form, str):
            form = json.loads(form)

        recipe = Recipe(
            dateRegistered=datetime.datetime.now(),
            name=form.get('name'),
            servings=form.get('servings'),
            preparationTime=form.get('preparationTime'),
            cookingTime=form.get('cookingTime'),
            difficulty=form.get('difficulty'),
            author=request.user,
            notes=form.get('notes')
        )
        recipe_form = RecipeForm(recipe.__dict__)

        steps = form.get('steps')
        ingredients = form.get('ingredients')

        if not recipe_form.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={'errors': recipe_form.errors})

        if len(steps) == 0:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={'errors': {'steps': [_('Steps cannot be empty')]}})

        if len(ingredients) == 0:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={'errors': {'ingredients': [_('Ingredients cannot be empty')]}})

        recipe.save()

        for step in steps:
            new_step = Step(
                recipe=recipe,
                number=step.get('number'),
                content=step.get('content')
            )
            try:
                new_step.save()
            except ValidationError as e:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data={'errors': e})

        for ing in ingredients:

            try:
                product = Product.objects.get(id=int(ing.get('product')))
            except ObjectDoesNotExist:
                recipe.delete()
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data={'errors': {'product': [_('A product on the recipe doesn\'t exist.')]}})

            try:
                unit = MeasurementUnit.objects.get(id=int(ing.get('unit')))
            except ObjectDoesNotExist:
                recipe.delete()
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data={'errors': {'unit': [_('A unit on the recipe doesn\'t exist.')]}})

            new_ing = Ingredient(
                recipe=recipe,
                product=product,
                amount=float(ing.get('amount')),
                unit=unit
            )
            ing_form = IngredientForm(new_ing.to_dict())

            if not ing_form.is_valid():
                recipe.delete()
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data={'errors': ing_form.errors})
            new_ing.save()

        RecipeLog.create(recipe)
        return Response(status=status.HTTP_200_OK)


class UnitsAutocomplete(APIView):
    @staticmethod
    def get(request):

        result = MeasurementUnit.objects.all()

        if request.GET.get('q'):
            result = result.filter(name__icontains=request.GET.get('q'))

        formatted_data = []

        for unit in result:
            formatted_data.append({
                'id': unit.id,
                'value': unit.name
            })

        return Response({'units': formatted_data}, status=status.HTTP_200_OK)

    def get_queryset(self):
        result = MeasurementUnit.objects.all()

        if self.request.GET.get('q'):
            result = result.filter(name__icontains=self.request.GET.get('q'))

        return result


class ValidateIngredient(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    @staticmethod
    def post(request):

        data = json.loads(request.data.get('ingredient'))

        try:
            ingredient = Ingredient(
                product=Product.objects.get(id=data.get('product')),
                amount=data.get('amount'),
                unit=MeasurementUnit.objects.get(id=data.get('unit'))
            )
        except ObjectDoesNotExist as e:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={'errors': str(e)})

        form = IngredientForm(ingredient.to_dict())

        if form.is_valid():
            return Response(status=status.HTTP_200_OK,
                            data=request.data.get('ingredient'))

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data={'errors': form.errors})


def return_errors(error_dict):
    return Response(status=status.HTTP_400_BAD_REQUEST, data={'errors': error_dict})
