from .models import Log, LogDetail, Category


def __recipe_log(category, recipe):
    log = Log.saved_log(category)

    LogDetail(head=log, title='recipe_id', content=recipe.id).save()
    LogDetail(head=log, title='recipe_name', content=recipe.name).save()
    LogDetail(head=log, title='user', content=recipe.author.email).save()

    return log


def create(recipe):
    log = __recipe_log(Category.RECIPE_CREATE, recipe)


def update(recipe):
    log = __recipe_log(Category.RECIPE_UPDATE, recipe)


def delete(recipe):
    log = __recipe_log(Category.RECIPE_DELETE, recipe)
