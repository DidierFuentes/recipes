from .models import Log, LogDetail, Category


def __google_log(category, user):
    log = Log.saved_log(category)

    LogDetail(head=log, title='user', content=user.email).save()
    return log


def login(user):
    log = __google_log(Category.GOOGLE_LOGIN, user)


def user_signup(user):
    log = __google_log(Category.GOOGLE_USER_SIGNUP, user)


def email_link(user):
    log = __google_log(Category.GOOGLE_EMAIL_LINK, user)
