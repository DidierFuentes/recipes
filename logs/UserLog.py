from .models import Log, LogDetail, Category


def __user_log(user):
    log = Log.saved_log(Category.USER_LOGIN)

    LogDetail(head=log, title='user', content=user.email).save()

    return log


def login(user):
    log = __user_log(user)
