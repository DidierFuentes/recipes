from .models import Log, LogDetail, Category


def __product_log(category, product, user):
    log = Log.saved_log(category)

    LogDetail(head=log, title='product_id', content=product.id).save()
    LogDetail(head=log, title='product_name', content=product.name).save()
    LogDetail(head=log, title='user', content=user.email).save()

    return log


def create(product, user):
    log = __product_log(Category.PRODUCT_CREATE, product, user)


def update(product, user):
    log = __product_log(Category.PRODUCT_UPDATE, product, user)


def delete(product, user):
    log = __product_log(Category.PRODUCT_DELETE, product, user)
