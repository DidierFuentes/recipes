from django.db import models
from tinymce import models as tinymce_models


class HelpPage(models.Model):
    title = models.CharField(max_length=500)
    url = models.CharField(max_length=200)
    last_edit = models.DateTimeField()
    content = tinymce_models.HTMLField()

    def __str__(self):
        return '<HelpPage(title={title}, url={url})>'.format(title=self.title, url=self.url)
