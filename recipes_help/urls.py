from django.conf.urls import url
from . import views

urlpatterns = [
    url('^$', views.HelpDirectoryView.as_view()),
    url('(?P<page>[a-zA-Z]*)/?$', views.HelpPageView.as_view())
]
