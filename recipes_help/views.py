from . import models

# Create your views here.
from django.views.generic import TemplateView


class HelpDirectoryView(TemplateView):
    template_name = "helpDirectory.html"

    def get_context_data(self, **kwargs):
        context = super(HelpDirectoryView, self).get_context_data(**kwargs)
        context['pages'] = models.HelpPage.objects.all().order_by('title')
        return context


class HelpPageView(TemplateView):
    template_name = 'helpPage.html'

    def get_context_data(self, **kwargs):
        page = models.HelpPage.objects.get(url=kwargs.get('page'))
        context = super(HelpPageView, self).get_context_data(**kwargs)

        context['content'] = page.content
        context['title'] = page.title
        context['last_edit'] = page.last_edit
        context['pages'] = models.HelpPage.objects.all().order_by('title')

        return context
