from django.contrib import admin
from django import forms

from .models import HelpPage

from tinymce.widgets import TinyMCE


class HelpPageForm(forms.ModelForm):

    content = forms.CharField(widget=TinyMCE(mce_attrs={'width': 700, 'theme': 'modern'},
                                             attrs={'width': 700, 'theme': 'modern'}))

    class Meta:
        model = HelpPage
        exclude = []


admin.site.register(HelpPage)
